#include <stdio.h>
#include <stdlib.h>

#define T unsigned int
#define SIZE 32
#define true 1 == 1
#define false 1 != 1
#define SZ 10

typedef int boolean;

int nCursor = -1;
T Stack[SIZE];


typedef struct 
{
	int pr;
	int dat;
} Node;


Node* arr[SZ];
int head;
int tail;
int items;

void initQueue()
{
	for (int i = 0; i < SZ; ++i)
		arr[i] = NULL;

	head = 0;
	tail = 0;
	items = 0;
}

void ins(int pr, int dat) 
{
	Node* node = (Node*)malloc(sizeof(Node));
	node->dat = dat;
	node->pr = pr;
	int flag;

	if (items != SZ) 
	{
		arr[tail++] = node;
		items++;
	}
	else
	{
		printf("%s \n", "Queue is full");
		return;
	}
}

Node* rem() 
{
	if (items == 0) 
	{
		return NULL;
	}
	else 
	{
		int idx, nChosenIdx;
		int i = head;
		idx = i++ % SZ;

		Node* tmp = arr[idx];
		nChosenIdx = idx;

		for (; i < SZ; ++i) 
		{
			idx = i % SZ;

			if (arr[idx] == NULL)
				continue;

			if (tmp == NULL)
			{
				tmp = arr[idx];
				nChosenIdx = idx;
				continue;
			}

			if (arr[idx]->pr > tmp->pr)
			{
				tmp = arr[idx];
				nChosenIdx = idx;
			}
		}

		for (int j = nChosenIdx; j < tail - 1; j++)
			arr[j] = arr[j + 1];
		
		arr[tail - 1] = NULL;
		tail--;
		items--;

		return tmp;
	}
}

void printQueue() 
{
	printf("[ ");

	for (int i = 0; i < SZ; ++i)
	{
		if (arr[i] == NULL)
			printf("[*, *] ");
		else
			printf("[%d, %d] ", arr[i]->pr, arr[i]->dat);
	}

	printf(" ]\n");
}

void prQueueTest() 
{
	initQueue();
	ins(1, 11);
	ins(3, 22);
	ins(4, 33);
	ins(2, 44);
	ins(3, 55);
	ins(4, 66);
	ins(5, 77);
	ins(1, 88);
	ins(2, 99);
	ins(6, 100);
	printQueue();

	for (int i = 0; i < 7; ++i) 
	{
		Node* n = rem();
		printf("pr=%d, dat=%d \n", n->pr, n->dat);
	}

	printQueue();

	ins(1, 110);
	ins(3, 120);
	ins(6, 130);
	printQueue();

	for (int i = 0; i < 5; ++i) 
	{
		Node* n = rem();
		printf("pr=%d, dat=%d \n", n->pr, n->dat);
	}

	printQueue();
}

boolean pushStack(T data) 
{
	if (nCursor < SIZE - 1) 
	{
		Stack[++nCursor] = data;
		return true;
	}
	else 
	{
		printf("%s \n", "Stack overflow");
		return false;
	}
}

T popStack() 
{
	if (nCursor != -1) 
		return Stack[nCursor--];
	else 
	{
		printf("%s \n", "Stack is empty");
		return -1;
	}
}

void vPrintNPopStack() 
{
	while (nCursor != -1) 
	{
		printf("%d ", popStack());
	}
}

int main()
{
	T insertedNumber;

	prQueueTest();

	printf("%s\n", "Insert a number");
	scanf("%d", &insertedNumber);

	do
	{
		pushStack(insertedNumber % 2);
		insertedNumber = insertedNumber >> 1 ;
	} while (insertedNumber != 0);

	vPrintNPopStack();

	return 0;
}